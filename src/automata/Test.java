package automata;

import java.io.*;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.Vector;


/**
 * @author verel
 *
 */
public class Test {

    /**
     * @param args
     */
    public static void main(String[] args) {
        //test();

        // Nombre maximale de fusiliers (taille maximale du réseau)
        int sizeMax = 20;
        int nTests = 1000000;

        Solution solution = new Solution(Initialization.nbRules);
        Solution bestSol = new Solution(Initialization.nbRules, -1);

        Initialization initialization = new Initialization();

        Automata automate = new Automata(sizeMax);
        initialization.initFreeRulesRandomly(solution);
        initialization.init(solution);
        solution.setFitness(automate.f(solution.getRules(), sizeMax));
        System.out.println("Fitness initial " + solution.getFitness());
        //System.out.println(solution.toString());
        //System.exit(0);
        for(int i=0; i<nTests; i++) {
            //initialization.initFreeRulesRandomly(solution); // random search
            solution.getNeighbour(solution); // hill climbing
            //initialization.initFreeRulesNeighbour(solution); // hill clinbing 2 ( ne pas utiliser)
            //solution.iterate(solution);//iterate
            //solution.recuitSimule(solution);//recuit simulé
            
            solution.setFitness(automate.f(solution.getRules(), sizeMax));
            //System.out.println(solution.toString());
            if(solution.getFitness() > bestSol.getFitness()) {
                bestSol.copySolution(solution);
            }
        }
System.out.println(bestSol.toString());
      //  System.out.print(solution.toString());
        String path = findPath();
        String defaultFileName = "solution_"+sizeMax;

        File file = new File(path+"svg/solution_"+sizeMax);
        file.mkdirs();

        int oldFit = getOldFitness(file.getAbsolutePath(), defaultFileName);

        if(oldFit < bestSol.getFitness()) {
            System.out.println("Sauvegarde du résultat...");
            saveBestResults(file.getAbsolutePath(), defaultFileName, automate, bestSol);
        } else System.out.println("Ancien fitness meilleur: "+oldFit);

        System.out.println("Fitness: "+bestSol.getFitness()+" (old: "+oldFit+")");
        System.out.println("The End.");
    }

    private static int getOldFitness(String filePath, String fileName) {
        try {
            Scanner scanner = new Scanner(new File(filePath+fileName+".dat"));
            if(scanner.hasNextInt()){
                return scanner.nextInt();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Impossible de trouver le fichier:");
            System.out.println("\t"+filePath+fileName+".dat");
        }
        return -1;
    }

    private static void saveBestResults(String filePath, String fileName, Automata automate, Solution solution) {
        PrintWriter ecrivain;
        try {
            ecrivain =  new PrintWriter(new BufferedWriter(new FileWriter(filePath + fileName + ".dat")));

            printToFile(solution.getFitness(), solution.getRules(), ecrivain);

            ecrivain.close();
        }
        catch (Exception e){
            System.out.println(e.toString());
        }

        int nFire;
        for(int i = 2; i <= automate.maxSize; i++) {
            // évolution de l'automate avec la règle rule sur un réseau de longueur i
            nFire = automate.evol(solution.getRules(), i);

            // affichage du nombre de fusiliers ayant tiré
            System.out.println("longueur " + i + " : " + nFire);

            // affiche la dynamique dans un fichier au format svg
            automate.exportSVG(i, 2 * i - 2, filePath + fileName + "_" + i + ".svg");
        }
    }

    private static String findPath() {
        try {
            return new File("").getCanonicalPath() + "/";
        } catch (IOException e) {
            e.printStackTrace();
            return Paths.get("").toString();
        }
    }

    public static void printToFile(int fitness, int [] rules, PrintWriter ecrivain) {
        ecrivain.print(fitness);
        for(int i = 0; i < Initialization.nbRules; i++) {
            ecrivain.print(" ");
            ecrivain.print(rules[i]);
        }
        ecrivain.println();
    }

    public static int [] initRulesFromFile(String fileName) {
        // 5 états + l'état "bord"
        int n = 5 + 1;

        int [] rules = new int[n * n * n];

        try {
            FileReader fichier = new FileReader(fileName);

            StreamTokenizer entree = new StreamTokenizer(fichier);

            int i = 0;
            while(entree.nextToken() == StreamTokenizer.TT_NUMBER) {
                rules[i] = (int) entree.nval;
                i++;
            }
            fichier.close();
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }

        return rules;
    }


    /*
    String outName = path + "out.dat";

        automata.Initialization init = new automata.Initialization();
        init.init(rules);

        PrintWriter ecrivain;
        try {
            ecrivain =  new PrintWriter(new BufferedWriter(new FileWriter(outName)));

            for(int i = 0; i < 1000; i++) {
                init.initFreeRulesRandomly(rules);
                fit = automate.f(rules, 19);

                printToFile(fit, rules, ecrivain);
            }

            ecrivain.close();
        }
        catch (Exception e){
            System.out.println(e.toString());
        }

     */

}

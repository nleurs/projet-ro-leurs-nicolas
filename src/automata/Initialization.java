package automata;

import java.util.Random;
import java.util.Vector;

/**
 * @author verel
 */
public class Initialization {

    public static final int nbRules = 216;
    public static Vector<Integer> freeRules;

    Random generator;

    public Initialization() {
        generator = new Random();
        freeRules = new Vector<>();
        saveIndexesOfFreeRules();
    }

    public void saveIndexesOfFreeRules() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                for (int k = 0; k < 4; k++) {
                    if ((i == 0 && j == 0 && k == 0) || (i == 1 && j == 1 && k == 1)) {

                    } else {
                        freeRules.add(toIndex(i, j, k));
                    }
                }
            }
        }

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if ((i == 1 && j == 0) || (i == 0 && j == 0) || (i == 1 && j == 1)) {

                } else {
                    freeRules.add(toIndex(5, i, j));
                }
            }
        }

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if ((i == 0 && j == 0) || (i == 1 && j == 0) || (i == 1 && j == 1)) {

                } else {
                    freeRules.add(toIndex(i, j, 5));

                }
            }
        }
    }


    public void init(Solution solution) {
        solution.setRuleAt(toIndex(0, 0, 0), 0);
        solution.setRuleAt(toIndex(5, 0, 0), 0);
        solution.setRuleAt(toIndex(0, 0, 5), 0);

        solution.setRuleAt(toIndex(1, 1, 1), 4);
        solution.setRuleAt(toIndex(5, 1, 1), 4);
        solution.setRuleAt(toIndex(1, 1, 5), 4);

        solution.setRuleAt(toIndex(5, 1, 0), 1);
        solution.setRuleAt(toIndex(1, 0, 5), 1);

        //solution.setRuleAt(toIndex(1, 0, 0), 2);
        //solution.setRuleAt(toIndex(2, 0, 0), 1);
        //solution.setRuleAt(toIndex(2, 0, 5), 2);
    }

    /*
     * Ecrit la regle
     *
     * g : etat de la cellule de gauche
     * c : etat de la cellule centrale
     * d : etat de la cellule de droite
     * r : etat de la cellule centale à t+1
     */

    private int toIndex(int g, int c, int d) {
        return g * 36 + c * 6 + d;
    }

    public void initFreeRulesRandomly(Solution sol) {
        for (Integer i : freeRules) {
            sol.setRuleAt(i, generator.nextInt(4));

        }
    }


    public void initFreeRulesNeighbour(Solution sol) {
        Solution neighbour = new Solution(sol);
        int sizeMax = 20;
        Automata automate = new Automata(sizeMax);
        do {
            neighbour.copySolution(sol);
            int i = generator.nextInt(freeRules.size());
            int r = generator.nextInt(4);
            neighbour.setRuleAt(i, r);
            neighbour.setFitness(automate.f(neighbour.getRules(), sizeMax));
        } while ((neighbour.getFitness() < sol.getFitness()) && neighbour.getRules()!=sol.getRules());
        sol.copySolution(neighbour);
    }
}
